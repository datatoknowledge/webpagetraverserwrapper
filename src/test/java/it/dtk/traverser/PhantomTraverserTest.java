package it.dtk.traverser;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;

import java.util.concurrent.Future;


/**
 * Created by fabiofumarola on 16/10/14.
 */
public class PhantomTraverserTest {

    private String url = "http://193.204.187.132:15000/traverse?url=";


    @Test
    public void testGoogle() {

        PhantomPageTraverser traverser = new PhantomPageTraverser(url);

        try {
            String html = traverser.traverse("http://www.google.it");
            assertNotNull(html);
            traverser.close();
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }

    }

    @Test
    public void testGoogleAsync() {

        PhantomPageTraverser traverser = new PhantomPageTraverser(url);

        try {
            Future<String> f = traverser.traverseAsync("http://www.google.it");
            assertNotNull(f.get());
            traverser.close();
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }

    }

}

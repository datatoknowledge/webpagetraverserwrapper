package it.dtk.traverser;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * Created by fabiofumarola on 15/10/14.
 */
public interface IPageTraverser {

    public String traverse(String url) throws Exception;

    public Future<String> traverseAsync(String url) throws IOException;

    public void close();

}

package it.dtk.traverser;

import java.util.Optional;

/**
 * Created by fabiofumarola on 15/10/14.
 */
public class TraverserFactory {

    public static String vpnUrl = "http://10.0.0.10:3000/traverse?url=";

    public static IPageTraverser getTraverser(Optional<String> baseUrl) {

        return new PhantomPageTraverser(baseUrl.orElse(vpnUrl));
    }
}

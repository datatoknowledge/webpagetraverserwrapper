package it.dtk.traverser;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.apache.commons.exec.PumpStreamHandler;

/**
 * @author fabiofumarola
 * @author Andrea Scarpino
 * 
 *         it uses the WebPageTraverser binary to extract given an url the
 *         augmented dom tree with other informations about rendering
 * 
 */
@Deprecated
public class PageTraverser {

	/**
	 * WebPageTraverser binary filename
	 */
	 private String webPageTraverser = "WebPageTraverser";


	/**
	 * 
	 */
	private static PageTraverser self;

	/**
	 * 
	 */
	private PageTraverser() {
	}

	/**
	 * 
	 * @return
	 */
	public static PageTraverser getIstance() {
		if (self == null) {
			self = new PageTraverser();
		}

		return self;
	}

	/**
	 * Get current WebPageTraverser filename
	 * 
	 * @return
	 */
	public String getWebPageTraverser() {
		return this.webPageTraverser;
	}

	/**
	 * Set WebPageTraverser filename
	 * 
	 * @param value
	 */
	public void setWebPageTraverser(String value) {
		this.webPageTraverser = value;
	}

	/**
	 * @param url
	 * @return
	 * @throws IOException
	 * @throws ExecuteException
	 */
	public String traverse(String url) throws ExecuteException, IOException {
		return traverse(url, false, null);
	}

	/**
	 * @param url
	 * @param json
	 * @return
	 * @throws IOException
	 * @throws ExecuteException
	 */
	public String traverse(String url, boolean json) throws ExecuteException,
			IOException {
		return traverse(url, json, null);
	}

	/**
	 * @param url
	 * @param json
	 * @param filename
	 * @return
	 * @throws IOException
	 * @throws ExecuteException
	 */
	public String traverse(String url, boolean json, String filename)
			throws ExecuteException, IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		ByteArrayOutputStream err = new ByteArrayOutputStream();

		CommandLine cmdLine = new CommandLine(webPageTraverser);
		cmdLine.addArgument(url);

		if (json) {
			cmdLine.addArgument("-j");
		}

		if (filename != null) {
			cmdLine.addArgument("-f");
			cmdLine.addArgument(filename);
		}

		DefaultExecutor executor = new DefaultExecutor();
		executor.setStreamHandler(new PumpStreamHandler(out, err));
		try {
			executor.execute(cmdLine);
		} catch (ExecuteException e) {
			throw e;
		} catch (IOException e) {
			throw e;
		}

		String output = null;
		if (filename == null) {
			output = out.toString();
		} else {
			File file = new File(filename);

			if (file.exists()) {
				output = new String(Files.readAllBytes(Paths.get(filename)));
			} else {
				throw new IOException("ERROR: No such output file: " + filename);
			}
		}

		return output;
	}

	/**Used for machines without graphical interface
	 * @param url
	 * @return
	 * @throws IOException
	 * @throws ExecuteException
	 */
	public String xvfbtraverse(String url) throws ExecuteException, IOException {
		return xvfbtraverse(url, false, null);
	}

	/**Used for machines without graphical interface
	 * @param url
	 * @param json
	 * @return
	 * @throws IOException
	 * @throws ExecuteException
	 */
	public String xvfbtraverse(String url, boolean json) throws ExecuteException,
			IOException {
		return xvfbtraverse(url, json, null);
	}

	/**Used for machines without graphical interface
	 * @param url
	 * @param json
	 * @param filename
	 * @return
	 * @throws IOException
	 * @throws ExecuteException
	 */
	public String xvfbtraverse(String url, boolean json, String filename)
			throws ExecuteException, IOException {
		//setWebPageTraverser("xvfb-run -a WebPageTraverser");
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		ByteArrayOutputStream err = new ByteArrayOutputStream();

		CommandLine cmdLine = new CommandLine("xvfb-run");
		cmdLine.addArgument("-a");
		cmdLine.addArgument(webPageTraverser);
		cmdLine.addArgument(url);

		if (json) {
			cmdLine.addArgument("-j");
		}

		if (filename != null) {
			cmdLine.addArgument("-f");
			cmdLine.addArgument(filename);
		}

		DefaultExecutor executor = new DefaultExecutor();
		executor.setStreamHandler(new PumpStreamHandler(out, err));
		try {
			executor.execute(cmdLine);
		} catch (ExecuteException e) {
			throw e;
		} catch (IOException e) {
			throw e;
		}

		String output = null;
		if (filename == null) {
			output = out.toString();
		} else {
			File file = new File(filename);

			if (file.exists()) {
				output = new String(Files.readAllBytes(Paths.get(filename)));
			} else {
				throw new IOException("ERROR: No such output file: " + filename);
			}
		}

		return output;
	}
}

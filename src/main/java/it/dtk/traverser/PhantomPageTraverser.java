package it.dtk.traverser;

import com.ning.http.client.AsyncCompletionHandler;
import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.AsyncHttpClientConfig;
import com.ning.http.client.AsyncHttpClientConfig.Builder;
import com.ning.http.client.Response;

import java.io.IOException;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * Created by fabiofumarola on 15/10/14.
 */
public class PhantomPageTraverser implements IPageTraverser{

    private final String baseUrl;


    private final AsyncHttpClient httpClient;


    PhantomPageTraverser(String baseUrl){
        final Builder builder = new AsyncHttpClientConfig.Builder();
        builder.setCompressionEnabled(true)
                .setAllowPoolingConnection(true)
                .setRequestTimeoutInMs(30000)
                .build();
        this.baseUrl = baseUrl;
        httpClient =  new AsyncHttpClient(builder.build());
    }

    @Override
    public String traverse(String url) throws IOException, ExecutionException, InterruptedException {
        Future<Response> f = httpClient.prepareGet(baseUrl +  url).execute();
        return f.get().getResponseBody();
    }

    @Override
    public Future<String> traverseAsync(String url) throws IOException {
        return httpClient.prepareGet(baseUrl + url).execute(new AsyncCompletionHandler<String>() {
            @Override
            public String onCompleted(Response response) throws Exception {
                return response.getResponseBody();
            }
        });
    }

    @Override
    public void close() {
        httpClient.closeAsynchronously();
    }
}

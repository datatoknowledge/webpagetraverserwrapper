WebPageTraverserWrapper
=======================

Description
-----------
WebPageTraverserWrapper is a [WebPageTraverser](https://bitbucket.org/wheretolive/webpagetraverser) Java wrapper.
From versione 1.3 supports a new version of WebPageTraverse made by using node.js and phantom.js. This project is named
[phantomwebtraverser](https://bitbucket.org/datatoknowledge/phantomwebtraverser). In order to use the new feature please
follows the installation guide in the project.

Dependencies
------------
* Java >= 1.7
* Maven >= 3.0
* commons-exec 1.1

Build and installation
----------------------
1. $ mvn compile
2. $ mvn install
